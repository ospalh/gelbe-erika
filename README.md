# Gelbe Erika

The font of an “[Erika](https://saechsischeschreibmaschinen.com/ideal-erika/)” or Erika E 115 tipewriter from the GDR from the 1980s.

I recently got miself a tipewriter, to fill out forms, write the addresses on envelopes and the like. It’s yellow, and it’s called Erika. I liked the font of it, so went looking for a digital version of it.

I did find  an “[Erika](http://www.peter-wiegel.de/ErikaType.html)” font, a [set](https://www.peter-wiegel.de/Fonts/index.html), actually, based on a GDR tipewriter called “Erika”. Unfortunately, it turns out that that one is about as different from the font of my Erika as two tipewriter fonts can be. It looks like the manufacturer changed the font at some time between 1967 and 1980. This can be seen in the [Stasi reports](https://www.ndr.de/geschichte/chronologie/stasiaktenlindenberg100_backId-udolindenbergstasi100.html) on Udo Lindenberg. When yu look at just the tipefaces, there are two quite distinct once. Peter Wiegel’s and that of my Erika.

So, what to do? Es gibt nichts gutes, außer man tut es. I went and carefully tiped every glif my Erika has, scand that, and used [FontForge](https://fontforge.org/en-US/) to create this font.
